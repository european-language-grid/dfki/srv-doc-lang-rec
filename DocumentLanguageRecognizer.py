'''
Created on 14.03.2019

@author: mamo06
'''
# -*- coding: utf-8 -*-

import os 
import operator
from collections import Counter
import LanguageRanking





class DLR(object):
    
    # fed knowledge stats
    bigram_train_stats = {}
    trigram_train_stats = {}
    vocab_train_all = {}
    
    # predicting stats
    bigram_predict_stats = []
    trigram_predict_stats = []
    vocab_predict = []
    
    
    def create_lan_stats(self, directory):  
        mypath = directory + "/" 
        file_list = os.listdir(mypath)
            
        for f in file_list:
            
            vocab = {}
            text = open(mypath + f, encoding="utf8").read()
            vocab = self.create_vocab(text, False)
    
            # calculate some n-gram figures
            self.bigram_train_stats[f[0:2]] = self.encoded_to_ngrams(text, 2)
            self.trigram_train_stats[f[0:2]] = self.encoded_to_ngrams(text, 3)
            self.vocab_train_all[f[0:2]] = vocab
        
    def recode_input(self, text):
        t = text
        return t
                   
    def create_vocab(self, text, predict):
        vocab = {}
        final_v = {}
        for token in text.split():
            try:
                vocab[token] = vocab[token]+1
            except:
                vocab[token] = 1
        
        if not predict:
            for key in vocab.keys():
                if vocab[key] >= 3:
                    final_v[key] = vocab[key]
        else:
            final_v = vocab
                
        sorted_vocab = sorted(final_v.items(), key=operator.itemgetter(1), reverse=True)
        return sorted_vocab
             
    def encoded_to_ngrams(self, text, n):
        grams = []
        for i in range(len(text)-2):
            #grams.append((text[i], text[i+1], text[i+2]))
            grams.append(text[i:i+n])
        
        # create a dictionary from the n-gram list   
        ngram_dict = Counter(list(grams)) 
        sorted_ngram_dict = sorted(ngram_dict.items(), key=operator.itemgetter(1), reverse=True) 
        return sorted_ngram_dict
        
        
           
    # main function    
    #if __name__ == '__main__':
    
    def run(self, inpt):
        
        # hand over directory of several files in several languages to create some language statistics
        self.create_lan_stats("data") 
        text = "abbiamo il know how le risorse ma il timing non é dei migliore"
        # input some sentence
        if len(inpt) > 0:
            text = inpt        
        
        #txt = recode_input(text)
        
        bigram_predict_stats = self.encoded_to_ngrams(text, 2)
        trigram_predict_stats = self.encoded_to_ngrams(text, 3)
        vocab_predict = self.create_vocab(text, True)
       
    
        ################ compare the training and the input statistics ################
          
        # Basic principle: The %rank% most frequent input tokens are mostly overlapping with those of which reference language?
        vocab_matches_dict = {}
        # iterate over the reference languages
        for lan in self.vocab_train_all.keys(): 
            # iterate the frequent words from the input (it is a list containing tuples: (key:val))
            for input_word in vocab_predict:
                rank_counter = 0 
                # iterate over the words from the test snippet
                for word in self.vocab_train_all[lan]:
                    rank_counter += 1
                    rank_counter_rel = len(self.vocab_train_all[lan])/100.0*rank_counter               
                    if input_word[0] == word[0]:
                        # the lower the rank, the more frequent the n-gram
                        if input_word[0] in vocab_matches_dict.keys() and rank_counter_rel < vocab_matches_dict[input_word[0]][1]: 
                            vocab_matches_dict[input_word[0]] = (lan, rank_counter_rel)
                        elif input_word[0] not in vocab_matches_dict.keys():
                            vocab_matches_dict[input_word[0]] = (lan, rank_counter_rel)
                            break
        
        #print vocab_matches_dict
        
        
        # The %rank% most frequent input bi-grams are mostly overlapping with those of which reference language?
        bigram_matches_dict = {}
        # iterate over the reference languages
        for lan in self.bigram_train_stats.keys(): 
            # iterate the frequent bi-gram from the input (it is a list containing tuples: (key:val))
            for input_bigram in bigram_predict_stats:
                rank_counter = 0
                # iterate over the bi-gram from the test snippet
                for bigram in self.bigram_train_stats[lan]:
                    rank_counter += 1
                    rank_counter_rel = len(self.bigram_train_stats[lan])/100.0*rank_counter
                    if input_bigram[0] ==bigram[0]:
                        
                        if input_bigram[0] in bigram_matches_dict.keys() and rank_counter_rel < bigram_matches_dict[input_bigram[0]][1]:
                            bigram_matches_dict[input_bigram[0]] = (lan, rank_counter_rel)
    
                        elif input_bigram[0] not in bigram_matches_dict.keys():
                            bigram_matches_dict[input_bigram[0]] = (lan, rank_counter_rel)
                            break
        
        #print bigram_matches_dict
                    
        
        # The %rank% most frequent input bi-grams are mostly overlapping with those of which reference language?
        trigram_matches_dict = {}
        # iterate over the reference languages
        for lan in self.trigram_train_stats.keys(): 
            # iterate the frequent bi-gram from the input (it is a list containing tuples: (key:val))
            for input_trigram in trigram_predict_stats:
                rank_counter = 0     
                # iterate over the tri-gram from the test snippet
                for trigram in self.trigram_train_stats[lan]:
                    rank_counter += 1
                    rank_counter_rel = len(self.trigram_train_stats[lan])/100.0*rank_counter                
                    if input_trigram[0] == trigram[0]:
                        
                        if input_trigram[0] in trigram_matches_dict.keys() and rank_counter_rel < trigram_matches_dict[input_trigram[0]][1]:
                            trigram_matches_dict[input_trigram[0]] = (lan, rank_counter_rel)
                            
                        elif input_trigram[0] not in trigram_matches_dict.keys():
                            trigram_matches_dict[input_trigram[0]] = (lan, rank_counter_rel)
                            break
            
        #print trigram_matches_dict
        
        lr = LanguageRanking.rank(vocab_matches_dict, bigram_matches_dict, trigram_matches_dict)
        
        return lr
  


if __name__ == '__main__':
    
    dlr = DLR()
    print(dlr.run(""))
    
    
    
