'''
Created on 14.03.2019

@author: mamo06
'''

import operator
from symbol import return_stmt

def rank(vocab_lang_cand, bigrams_lang_cand, trigrams_lang_cand):
    
    return_string = ""
    appr1 = sortByHits(vocab_lang_cand)
    appr2 = sortByHits(bigrams_lang_cand)
    appr3 = sortByHits(trigrams_lang_cand)
    
    try:
        if appr1[0][0] == appr2[0][0] and appr2[0][0] == appr3[0][0]:
            return_string = "The language is most probably: " + appr1[0][0]
        elif appr1[0][0] == appr3[0][0]:
            return_string = "The language is quite probably: " + appr1[0][0]
        elif appr1[0][0] == appr2[0][0] or appr2[0][0] == appr3[0][0]:
            return_string = "The language is quite probably: " + appr2[0][0]
        else:
            return_string = "I can not identify the language. Sorry!"
            return_string = "The training data is probably too noisy or the reference input is too short."
            return_string = "However, here is a ranking of possible candidates, listing most probable ones first:"
            lst = countCandidates(appr1[0:3], appr2[0:3], appr3[0:3])
            for val in lst:
                return_string = return_string + val[0] + " "            
    except IndexError: 
        return_string = "Please insert a bit more text (to increase the probability to hit a word in the reference corpora)."
        
    return return_string


def sortByHits(gram_lang_candidates):
    candidates = {}
    for k in gram_lang_candidates.keys():
        val = gram_lang_candidates[k][0]
        try:
            candidates[val] = candidates[val]+1
        except:
            candidates[val] = 1     
    return sorted(candidates.items(), key=operator.itemgetter(1), reverse=True)
        
        
def countCandidates(a1, a2, a3):
    dict_lan = {}
    for tup in a1:
        try:
            dict_lan[tup[0]] = dict_lan[tup[0]]+tup[1]
        except:
            dict_lan[tup[0]] = tup[1]
            
    for tup in a2:
        try:
            dict_lan[tup[0]] = dict_lan[tup[0]]+tup[1]
        except:
            dict_lan[tup[0]] = tup[1]
            
    for tup in a3:
        try:
            dict_lan[tup[0]] = dict_lan[tup[0]]+tup[1]
        except:
            dict_lan[tup[0]] = tup[1]
    
    return sorted(dict_lan.items(), key=operator.itemgetter(1), reverse=True)
    
    
    