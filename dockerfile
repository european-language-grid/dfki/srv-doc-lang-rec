from python:3.6.4-slim-jessie

RUN pip install CherryPy
COPY DocumentLanguageRecognizer.py .
COPY LanguageRanking.py .
COPY dlr_ws.py .
COPY data ./data/
EXPOSE 8080
ENTRYPOINT ["python", "dlr_ws.py"]